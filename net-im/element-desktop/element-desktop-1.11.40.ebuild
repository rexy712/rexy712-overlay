# Copyright 2020 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="A glossy Matrix collaboration client for desktop"
HOMEPAGE="https://element.io"

inherit desktop

if [ "${PR}" != "r0" ];then
	rc_num="${PR:1}"
	MY_PV="${PV}-rc.${rc_num}"
else
	MY_PV="${PV}"
fi
MY_PF="${PN}-${MY_PV}"

S="${WORKDIR}"
ELEMENT_WEB_V="element-web-${MY_PV}"
ELEMENT_DESKTOP_V="element-desktop-${MY_PV}"
ELEMENT_WEB="element-web"
ELEMENT_DESKTOP="element-desktop"
WEB_S="${S}/${ELEMENT_WEB}"
DESKTOP_S="${S}/${ELEMENT_DESKTOP}"

SRC_URI="
	https://github.com/vector-im/element-web/archive/v${MY_PV}.tar.gz -> ${ELEMENT_WEB_V}.tar.gz
	https://github.com/vector-im/element-desktop/archive/v${MY_PV}.tar.gz -> ${ELEMENT_DESKTOP_V}.tar.gz
"
KEYWORDS="~amd64"

LICENSE="Apache-2.0"
SLOT="0"
IUSE="+emoji proxy native-modules"
REQUIRED_USE=""
RESTRICT="network-sandbox" #need network sandbox disabled for `yarn install` to work

COMMON_DEPEND="
	!net-im/element-desktop-bin
	>=net-libs/nodejs-13.0
	>=sys-apps/yarn-1.22
	x11-libs/libXScrnSaver
	net-print/cups
	dev-libs/nss
	gnome-base/gconf
	>=media-video/ffmpeg-4.0
	native-modules? (
	                  dev-lang/tcl:=
	                  dev-db/sqlcipher:=
	)
"
DEPEND="
	${COMMON_DEPEND}
	native-modules? (
	                  || ( dev-lang/rust-bin dev-lang/rust )
	                  sys-devel/make
	)
"
RDEPEND="
	${COMMON_DEPEND}
	emoji? ( >=media-fonts/noto-emoji-20180823 )
"

PATCHES=()

DESTINATION="/opt/Element"

get_proxy() {
	if ! use "proxy";then
		echo ""
	else
		echo "$http_proxy"
	fi
}

src_unpack() {
	unpack "${ELEMENT_WEB_V}.tar.gz" || die
	unpack "${ELEMENT_DESKTOP_V}.tar.gz" || die
	mv "${S}/${ELEMENT_WEB_V}" "${S}/${ELEMENT_WEB}" || die
	mv "${S}/${ELEMENT_DESKTOP_V}" "${S}/${ELEMENT_DESKTOP}" || die
}

src_prepare() {
	my_proxy="$(get_proxy)"

	cd "${WEB_S}" || die

	#workaround for canvas-prebuilt not existing for node 15
	http_proxy="$my_proxy" yarn add node-gyp || die "Yarn module installation failed"

	http_proxy="$my_proxy" yarn install || die "Yarn module installation failed"

	cd "${DESKTOP_S}" || die
	http_proxy="$my_proxy" yarn install || die "Yarn module installation failed"
	ln -s "${WEB_S}/webapp" "${DESKTOP_S}/" || die

	cd "${S}" || die
	default
}

src_compile() {
	my_proxy="$(get_proxy)"

	einfo "Building element-web..."
	cd "${WEB_S}" || die
	yarn build || die "Element web build failed"

	einfo "Building element-desktop..."
	cd "${DESKTOP_S}" || die
	http_proxy="$my_proxy" yarn build || die "Element desktop build failed"

	if use native-modules;then
		einfo "Building native modules..."
		http_proxy="$my_proxy" yarn run build:native || die
	fi
}

src_install() {
	insinto "${DESTINATION}"
	cp "${FILESDIR}/${PN}.desktop" "${T}"
	sed -e 's@{::install_location::}@'"${DESTINATION}"'@g' "${T}/${PN}.desktop" -i
	domenu "${T}/${PN}.desktop"
	cp -r "${DESKTOP_S}/dist/linux-unpacked/"* "${D}/${DESTINATION}/"

	insinto "${DESTINATION}/resources/"
	doins -r "${WEB_S}/webapp/"

	insinto "${DESTINATION}/resources/webapp/"
	doins "${DESKTOP_S}/element.io/release/config.json"

	dosym "${DESTINATION}/${PN}" "/usr/bin/${PN}"
}


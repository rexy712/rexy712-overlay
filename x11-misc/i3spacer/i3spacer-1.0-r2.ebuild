# Copyright 2019 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit cmake

DESCRIPTION="A transparent window to act as a spacer in x11-wm/i3 or x11-wm/i3gaps"
HOMEPAGE="https://gitlab.com/rexy712/i3spacer"
MY_PV="v${PV}"
MY_P="${PN}-${MY_PV}"
SRC_URI="https://gitlab.com/rexy712/i3spacer/-/archive/${MY_PV}/${PN}-${MY_PV}.tar.gz"
S="${WORKDIR}/${MY_P}"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="+decorate"

RDEPEND="x11-libs/gtk+:3[X]"
DEPEND="${RDEPEND}"
BDEPEND="
         dev-build/cmake
         virtual/pkgconfig
"

src_configure(){
	local mycmakeargs=(-DDECORATE_WINDOW=$(usex decorate ON OFF))
	cmake_src_configure
}


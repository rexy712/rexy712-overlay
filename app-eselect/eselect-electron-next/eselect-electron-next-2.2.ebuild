# Copyright 2019 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=7

DESCRIPTION="Manages versions of electron executables"
HOMEPAGE="https://www.gentoo.org/"
SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	app-admin/eselect
	!app-eselect/eselect-electron
"
RDEPEND="${DEPEND}"

S="${FILESDIR}"

src_install() {
	insinto /usr/share/eselect/modules
	newins "${FILESDIR}"/electron-${PVR}-next.eselect electron.eselect
}

# Copyright 2020 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit cmake

DESCRIPTION="A faster lolcat with GNU cat options written in C++"
HOMEPAGE="https://gitlab.com/rexy712/roflcat"
MY_PV="v${PV}"
MY_P="${PN}-${MY_PV}"
SRC_URI="https://gitlab.com/rexy712/roflcat/-/archive/${MY_PV}/roflcat-${MY_PV}.tar.gz"
S="${WORKDIR}/${MY_P}"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="sys-libs/ncurses"
DEPEND="${RDEPEND}
        dev-build/cmake"
BDEPEND=""


# Copyright 2021 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit cmake-multilib

MY_PV="v${PV}"
MY_P="${PN}-${MY_PV}"
S="${WORKDIR}/${MY_P}"

DESCRIPTION="Rexy's personal library of misc stuff"
HOMEPAGE="https://gitlab.com/rexy712/rexylib"
SRC_URI="https://gitlab.com/rexy712/rexylib/-/archive/${MY_PV}/rexylib-${MY_PV}.tar.gz"

LICENSE="GPL-3"
SLOT="$(ver_cut 1)/$(ver_cut 2)"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs"

RDEPEND=""
DEPEND="${RDEPEND}
        dev-build/cmake"
BDEPEND=""

src_prepare(){
	cmake_src_prepare
}
src_configure(){
	local mycmakeargs=(
		-DENABLE_SHARED=$(usex static-libs OFF ON)
		-DBUILD_TESTS=OFF
	)
	cmake-multilib_src_configure
}

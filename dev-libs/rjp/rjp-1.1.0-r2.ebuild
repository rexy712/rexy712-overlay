# Copyright 2020 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit cmake-multilib

MY_PV="v${PV}"
MY_P="${PN}-${MY_PV}"
S="${WORKDIR}/${MY_P}"

DESCRIPTION="A simplistic json parser written in C"
HOMEPAGE="https://gitlab.com/rexy712/rjp"
SRC_URI="https://gitlab.com/rexy712/rjp/-/archive/${MY_PV}/rjp-${MY_PV}.tar.gz"

LICENSE="GPL-3"
SLOT="$(ver_cut 1)/$(ver_cut 2)"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs debug +c++"

RDEPEND=""
DEPEND="${RDEPEND}
        dev-build/cmake
				c++? ( >=dev-libs/rexylib-0.2.0:= )"
BDEPEND=""

src_prepare(){
	cmake_src_prepare
}
src_configure(){
	local mycmakeargs=(
		-DENABLE_SHARED=$(usex static-libs OFF ON)
		-DENABLE_DIAGNOSTICS=$(usex debug ON OFF)
		-DENABLE_C++=$(usex c++ ON OFF)
	)
	cmake-multilib_src_configure
}

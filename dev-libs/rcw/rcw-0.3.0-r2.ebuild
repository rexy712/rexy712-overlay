# Copyright 2021 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit cmake-multilib

MY_PV="v${PV}"
MY_P="${PN}-${MY_PV}"
S="${WORKDIR}/${MY_P}"

DESCRIPTION="A libcurl wrapper for C++"
HOMEPAGE="https://gitlab.com/rexy712/rcw"
SRC_URI="https://gitlab.com/rexy712/rcw/-/archive/${MY_PV}/rcw-${MY_PV}.tar.gz"

LICENSE="AGPL-3"
SLOT="$(ver_cut 1)/$(ver_cut 2)"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs"

RDEPEND="
        net-misc/curl[ssl]
        >=dev-libs/rexylib-0.2.0:="
DEPEND="${RDEPEND}
        dev-build/cmake"
BDEPEND=""

src_prepare(){
	cmake_src_prepare
}
src_configure(){
	local mycmakeargs=(
		-DENABLE_SHARED=$(usex static-libs OFF ON)
		-DBUILD_TESTS=OFF
	)
	cmake-multilib_src_configure
}

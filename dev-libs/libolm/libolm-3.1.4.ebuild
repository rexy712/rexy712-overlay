# Copyright 2020 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit cmake-multilib

MY_P="olm"
S="${WORKDIR}/${MY_P}-${PV}"

DESCRIPTION="Implementation of the olm and megolm cryptographic ratchets"
HOMEPAGE="https://gitlab.matrix.org/matrix-org/olm"
SRC_URI="https://gitlab.matrix.org/matrix-org/olm/-/archive/${PV}/${MY_P}-${PV}.tar.gz"

LICENSE="Apache-2.0"
SLOT="3/1"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs test"

RDEPEND=""
DEPEND="${RDEPEND}
        dev-build/cmake
	dev-build/ninja
"
BDEPEND=""

src_prepare(){
	cmake_src_prepare
}
src_configure(){
	local mycmakeargs=(
		-DOLM_TESTS=$(usex test ON OFF)
		-DBUILD_SHARED_LIBS=$(usex static-libs OFF ON)
	)
	cmake-multilib_src_configure
}

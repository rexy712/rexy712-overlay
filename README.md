An overlay for my personal projects and some other utilities not found elsewhere. Makes it easier to install them and lets portage track them for me.

#### To install with layman:  
`sudo layman -o https://rexy712.chickenkiller.com/gitea/rexy712/rexy712-overlay/raw/branch/master/repos.xml -f -a rexy712-overlay`

#### Currently provides:  
* app-doc/opengl-refpages
* dev-libs/libolm
* dev-libs/rjp
* dev-libs/rexylib
* dev-libs/rcw
* sys-power/rexbacklight
* sys-power/rexledctl
* sys-apps/strlen
* sys-apps/rex-edid
* x11-misc/i3spacer
* games-misc/roflcat
* net-im/element-desktop


#### And some that I will no longer be maintaining regularly:  
* app-eselect/eselect-electron-next
* dev-util/electron-bin-next
* dev-util/electron-next


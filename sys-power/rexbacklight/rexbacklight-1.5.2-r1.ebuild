# Copyright 2021 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit cmake

DESCRIPTION="A replacement for xbacklight with some added functionality"
HOMEPAGE="https://gitlab.com/rexy712/rexbacklight"
MY_PV="v${PV}"
MY_P="${PN}-${MY_PV}"
SRC_URI="https://gitlab.com/rexy712/rexbacklight/-/archive/${MY_PV}/rexbacklight-${MY_PV}.tar.gz"
S="${WORKDIR}/${MY_P}"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+udev +restore compat"

RDEPEND="
	restore? ( >=dev-libs/rjp-1.0.0 )
	udev? ( virtual/udev )"
DEPEND="${RDEPEND}
	dev-build/cmake"
BDEPEND=""

src_prepare(){
	cmake_src_prepare
}
src_configure(){
	local mycmakeargs=(-DBUILD_REXLEDCTL=OFF -DBUILD_REXBACKLIGHT=ON)
	if use restore;then
		mycmakeargs+=(-DENABLE_RESTORE_FILE=ON)
	else
		mycmakeargs+=(-DENABLE_RESTORE_FILE=OFF)
	fi
	if use udev;then
		mycmakeargs+=(-DINSTALL_UDEV_BACKLIGHT_RULE=ON -DUDEV_DIR="/lib/udev/rules.d")
	else
		mycmakeargs+=(-DINSTALL_UDEV_BACKLIGHT_RULE=OFF)
	fi
	if use compat;then
		mycmakeargs+=(-DXBACKLIGHT_COMPAT=ON)
	else
		mycmakeargs+=(-DXBACKLIGHT_COMPAT=OFF)
	fi
	cmake_src_configure
}

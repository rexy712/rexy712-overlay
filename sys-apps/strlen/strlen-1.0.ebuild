# Copyright 2019 rexy712
# Distributed under the terms of the GNU General Public License v3

EAPI=7

DESCRIPTION="Terminal program to utilize C library's strlen"
HOMEPAGE="https://gitlab.com/rexy712/strlen"
MY_PV="v${PV}"
MY_P="${PN}-${MY_PV}"
SRC_URI="https://gitlab.com/rexy712/strlen/-/archive/${MY_PV}/strlen-${MY_PV}.tar.gz"
S="${WORKDIR}/${MY_P}"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"
BDEPEND=""

src_configure(){
	return
}
src_compile(){
	emake all
}



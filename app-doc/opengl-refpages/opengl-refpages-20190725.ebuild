# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="OpenGL and OpenGL ES reference page sources"
HOMEPAGE="https://github.com/KhronosGroup/OpenGL-Refpages"
SRC_URI="https://www.archlinux.org/packages/extra/any/opengl-man-pages/download/ -> ${P}.tar.zstd"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="
	app-arch/zstd
	dev-libs/libxslt
	app-text/docbook-xsl-stylesheets
"
S="${WORKDIR}"

src_unpack() {
	tar xf "${DISTDIR}/${P}.tar.zstd"
}
src_configure() {
	cd "usr/share/man/man3/" || die
	for file in *.3G.gz;do
		gunzip "$file" || die
	done
}
src_install() {
   cd "usr/share/man/man3/" || die
   doman *.3G
}
